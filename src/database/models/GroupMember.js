const path = require('path');
const CustomModel = require('./CustomModel');

class GroupMember extends CustomModel {
  static get tableName() {
    return 'groupMember';
  }

  $beforeInsert() {
    this.createdAt = new Date().toISOString();
    this.updatedAt = new Date().toISOString();
  }

  $beforeUpdate() {
    this.updatedAt = new Date().toISOString();
  }

  static get relationMappings() {
    return {
      user: {
        relation: CustomModel.BelongsToOneRelation,
        modelClass: path.join(__dirname, '/User'),
        join: {
          from: 'groupMember.userId',
          to: 'user.id'
        }
      },
      group: {
        relation: CustomModel.BelongsToOneRelation,
        modelClass: path.join(__dirname, '/Group'),
        join: {
          from: 'groupMember.groupId',
          to: 'group.id'
        }
      }
    };
  }
}

module.exports = GroupMember;

const Hapi = require('hapi');
const path = require('path');
const Dotenv = require('dotenv');
const Inert = require('inert');
const Vision = require('vision');
const hapiI18n = require('hapi-i18n');
const HapiSwagger = require('hapi-swagger');
const hapiAuthJWT = require('hapi-auth-jwt2');
const Good = require('good');
const routes = require('./api/routes');

// import environment variables from local secrets.env file
Dotenv.config({
  path: path.resolve(__dirname, 'secrets.env')
});

// create new server instance
const server = new Hapi.Server({
  host: process.env.APP_HOST || 'localhost',
  port: process.env.PORT || 8080,
  routes: {
    cors: true,
    validate: {
      failAction: async (request, h, err) => {
        if (process.env.NODE_ENV === 'production') {
          // In prod, log a limited error message and throw the default Bad Request error.
          throw err;
        } else {
          // During development, log and respond with the full error.
          throw err;
        }
      }
    }
  }
});

// multiple language. Use: request.i18n__(key)
const i18nOption = {
  locales: ['en', 'vi'],
  directory: `${__dirname}/locales`,
  languageHeaderField: 'language',
  queryParameter: 'lang',
  defaultLocale: 'en'
};

/* eslint-disable */
const validateUser = (decoded, request) => {
  if (decoded && decoded.id) {
    return {
      isValid: true
    };
  }
  return {
    isValid: false
  };
};
/* eslint-enable */

const apiVersionOptions = {
  basePath: '/api',
  validVersions: [1, 2],
  defaultVersion: 1,
  vendorName: 'api'
};

const swaggerOptions = {
  pathPrefixSize: 3,
  host: process.env.HOST,
  basePath: apiVersionOptions.basePath,
  info: {
    title: 'Blood Donation API Documentation',
    description: 'This is a Blood Donation API documentation.'
  },
  deReference: false
};

/* eslint-disable */
server.events.on(
  { name: 'request', channels: 'error' },
  (request, event, tags) => {
    console.info(`Request ${event.request} failed`);
  }
);
/* eslint-enable */

// add plugins
async function start() {
  try {
    // register plugins to server instance
    await server.register([
      Inert,
      Vision,
      {
        plugin: HapiSwagger,
        options: swaggerOptions
      },
      {
        plugin: Good,
        options: {
          reporters: {
            console: [
              {
                module: 'good-squeeze',
                name: 'Squeeze',
                args: [
                  {
                    response: '*',
                    log: '*'
                  }
                ]
              },
              {
                module: 'good-console'
              },
              'stdout'
            ]
          }
        }
      },
      {
        plugin: hapiI18n,
        options: i18nOption
      },
      hapiAuthJWT
    ]);

    server.auth.strategy('jwt', 'jwt', {
      key: process.env.JWT_SECRET || 'enouvo123',
      validate: validateUser,
      verifyOptions: {
        ignoreExpiration: true,
        algorithms: ['HS256']
      }
    });
    server.auth.default('jwt');
    server.route(routes);
    await server.start();
  } catch (error) {
    process.exit(1);
  }
  // eslint-disable-next-line
  console.info('Server running at:', server.info.uri);
}

start();

process.on('uncaughtException', err => {
  // eslint-disable-next-line
  console.info(err, 'Uncaught exception');
  process.exit(1);
});

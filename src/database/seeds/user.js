const bcrypt = require('bcrypt');

const Models = require('../models');

exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('users')
    .del()
    .then(() => knex('role').del())
    .then(() => knex('userInfo').del())
    .then(async () => {
      return await Models.Role.query().insertGraph([
        {
          name: 'superadmin',
          description: 'Admin has all the power'
        },
        {
          name: 'admin',
          description: 'Admin'
        },
        {
          name: 'user',
          description: 'The end user'
        }
      ])
      }
    )
    .then(async (roles) => {
      return await Models.User.query().insertGraph([
        {
          email: 'superadmin@blood.com',
          username: 'superadmin',
          password: bcrypt.hashSync('enouvo123', 5),
          fullName: 'Super Admin',
          roleId: roles[0].id
        },
        {
          email: 'admin@blood.com',
          username: 'admin',
          password: bcrypt.hashSync('enouvo123', 5),
          fullName: 'Admin',
          roleId: roles[1].id
        }
      ])
      }
    )
    .then(async (users) => {
      await Models.UserInfo.query().insertGraph([
          {
            id: 1,
            userId: users[0].id,
            gender: 'Nam',
            birthday: new Date(2018, 11, 8).toISOString(),
            cmnd: 197353241,
            facebook: 'https://www.facebook.com/longnt189',
            address: '15 Tạ Mỹ Duật, quận Sơn Trà, TP.Đà nẵng',
            job: 'coder',
            company: 'enouvo',
            donateTime: 'Toàn thời gian',
            donateStatus: 'Chỉ hiến toàn phần',
            bloodGroup: 'O',
            rh: 'rh+',
            plt: 56,
            rbc: 3,
            wbc: 34,
            hgb: 90,
            mcv: 78,
            hct: 68,
            mch: 65,
            mchc: 34,
            height: 76,
            weight: 56,
            bloodPressure: 546,
            bloodVessel: 34,
            venTo: true,
            contact: true,
            clinical: 'Bình thường',
            subclinical: 'Bình thường'
          },
          {
            id: 2,
            userId: users[1].id,
            gender: 'Nữ',
            birthday: new Date(2018, 11, 10).toISOString(),
            cmnd: 197353242,
            facebook: 'https://www.facebook.com/enouvo',
            address: '16 Tạ Mỹ Duật, quận Sơn Trà, TP.Đà nẵng',
            job: 'manage',
            company: 'enouvoSpace',
            donateTime: 'Toàn thời gian',
            donateStatus: 'Chỉ hiến toàn phần',
            bloodGroup: 'A',
            rh: 'rh-',
            plt: 34,
            rbc: 65,
            wbc: 34,
            hgb: 234,
            mcv: 23,
            hct: 34,
            mch: 234,
            mchc: 34,
            height: 55,
            weight: 56,
            bloodPressure: 87,
            bloodVessel: 78,
            venTo: true,
            contact: true,
            clinical: 'Bình thường',
            subclinical: 'Bình thường'
          }
        ])
      }
    );
};

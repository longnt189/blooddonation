const { Model } = require('objection');
const knexPostgis = require('knex-postgis');
const knex = require('../connection');

/* eslint no-underscore-dangle: 0 */
Model.knex(knex);

const st = knexPostgis(knex);

module.exports = {
  Model,
  st,
  knex
};

exports.up = knex =>
  knex.schema.createTable('donatePlace', table => {
    table.increments('id').primary();
    table.string('name');
    table.string('address');
    table.string('note');
    table.timestamp('createdAt');
    table.timestamp('updatedAt');
  });

exports.down = knex => knex.schema.dropTableIfExists('donatePlace');

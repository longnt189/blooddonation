const path = require('path');
const CustomModel = require('./CustomModel');

class UserInfo extends CustomModel {
  static get tableName() {
    return 'userInfo';
  }

  static get relationMappings() {
    return {
      user: {
        relation: CustomModel.BelongsToOneRelation,
        modelClass: path.join(__dirname, '/User'),
        join: {
          from: 'userInfo.userId',
          to: 'users.id'
        }
      }
    };
  }
}

module.exports = UserInfo;

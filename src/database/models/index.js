const Role = require('./Role');
const User = require('./User');
const UserInfo = require('./UserInfo');
const Group = require('./Group');
const GroupMember = require('./GroupMember');
const BloodDonation = require('./BloodDonation');
const BloodDonationRegister = require('./BloodDonationRegister');
const DonatePlace = require('./DonatePlace');

module.exports = {
  Role,
  User,
  UserInfo,
  Group,
  GroupMember,
  BloodDonation,
  BloodDonationRegister,
  DonatePlace
};

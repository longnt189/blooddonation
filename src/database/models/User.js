const path = require('path');
const CustomModel = require('./CustomModel');

class User extends CustomModel {
  static get tableName() {
    return 'users';
  }

  static get $hidden() {
    return ['password'];
  }

  $beforeInsert() {
    this.createdAt = new Date().toISOString();
    this.updatedAt = new Date().toISOString();
  }

  $beforeUpdate() {
    this.updatedAt = new Date().toISOString();
  }

  static get relationMappings() {
    return {
      role: {
        relation: CustomModel.BelongsToOneRelation,
        modelClass: path.join(__dirname, '/Role'),
        join: {
          from: 'users.roleId',
          to: 'role.id'
        }
      },
      userInfo: {
        relation: CustomModel.HasOneRelation,
        modelClass: path.join(__dirname, '/UserInfo'),
        join: {
          from: 'users.id',
          to: 'userInfo.userId'
        }
      },
      bloodDonations: {
        relation: CustomModel.HasManyRelation,
        modelClass: path.join(__dirname, '/BloodDonation'),
        join: {
          from: 'users.id',
          to: 'bloodDonation.userId'
        }
      }
    };
  }
}

module.exports = User;

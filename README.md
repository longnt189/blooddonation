# Blood Donation — Starter Files

## Requirements

> Node.js v8.x with `async/await`
>
> This version of Futureflix uses **hapi v17**. For hapi v16, use a [`1.x` release](https://github.com/fs-opensource/futureflix-starter-kit/releases)

The Futureflix Starter Kit uses hapi v17 and has full `async/await` support.

**Requirements**

- Node.js **v8.x** or later
- NPM/Yarn to install the project’s dependencies

## Setup and Run

To run your own Futureflix instance, clone this repository, install the dependencies, start a MongoDB instance in a Vagrant VM or on your own machine.

```bash
# clone repository
git@gitlab.com:enouvo/blooddonation-api.git
cd project

# install dependencies
npm i

# create your secrets.env file from secrets.env.example
cp secrets.env.sample secrets.env

# import sample data
npm run pumpitup

# start the server
npm start

# that’s it :)
```

The starter kit doesn’t contain any logging. If you don’t see any errors while starting the `server.js`,
[visit localhost:3000](http://localhost:3000). Have fun!

## Thank You with a Hug!

It’s great to see you exploring this repository. Really! Dig through the code and hopefully you’ll take wins away ❤️

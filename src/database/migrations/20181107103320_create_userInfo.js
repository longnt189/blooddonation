exports.up = knex =>
  knex.schema.createTable('userInfo', table => {
    table.increments('id').primary();
    table.integer('userId');
    table
      .foreign('userId')
      .references('users.id')
      .onDelete('CASCADE');
    table.string('gender');
    table.timestamp('birthday');
    table.integer('cmnd').unique();
    table.string('facebook');
    table.string('address');
    table.string('job');
    table.string('company');
    table.string('donateTime');
    table.string('donateStatus');
    table.timestamp('donateDelayTime');
    table.string('bloodGroup');
    table.string('rh');
    table.integer('plt');
    table.integer('rbc');
    table.integer('wbc');
    table.integer('hgb');
    table.integer('mcv');
    table.integer('hct');
    table.integer('mch');
    table.integer('mchc');
    table.integer('height');
    table.integer('weight');
    table.integer('bloodPressure');
    table.integer('bloodVessel');
    table.boolean('venTo');
    table.boolean('contact');
    table.string('clinical');
    table.string('subclinical');
  });

exports.down = knex => knex.schema.dropTableIfExists('userInfo');

exports.up = knex =>
  knex.schema.createTable('role', table => {
    table.increments('id').primary();
    table
      .string('name')
      .notNullable()
      .unique();
    table.string('description').notNullable();
  });

exports.down = knex => knex.schema.dropTableIfExists('role');

const USER_ROLE = {
  SUPER_ADMIN: 1,
  ADMIN: 2,
  USER: 3
};

module.exports = {
  USER_ROLE
};

const path = require('path');
const CustomModel = require('./CustomModel');

class BloodDonationRegister extends CustomModel {
  static get tableName() {
    return 'bloodDonationRegister';
  }

  $beforeInsert() {
    this.createdAt = new Date().toISOString();
    this.updatedAt = new Date().toISOString();
  }

  $beforeUpdate() {
    this.updatedAt = new Date().toISOString();
  }

  static get relationMappings() {
    return {
      user: {
        relation: CustomModel.HasOneRelation,
        modelClass: path.join(__dirname, '/User'),
        join: {
          from: 'bloodDonationRegister.userId',
          to: 'user.id'
        }
      }
    };
  }
}

module.exports = BloodDonationRegister;

const CustomModel = require('./CustomModel');

class DonatePlace extends CustomModel {
  static get tableName() {
    return 'donatePlace';
  }
}

module.exports = DonatePlace;

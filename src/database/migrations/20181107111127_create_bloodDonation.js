exports.up = knex =>
  knex.schema.createTable('bloodDonation', table => {
    table.increments('id').primary();
    table.integer('userId');
    table
      .foreign('userId')
      .references('users.id')
      .onDelete('CASCADE');
    table.integer('donatePlaceId');
    table
      .foreign('donatePlaceId')
      .references('donatePlace.id')
      .onDelete('CASCADE');
    table.timestamp('donateTime');
    table.string('donateMethod');
    table.text('note');
    table.timestamp('createdAt');
    table.timestamp('updatedAt');
  });

exports.down = knex => knex.schema.dropTableIfExists('bloodDonation');

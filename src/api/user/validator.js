const Joi = require('joi');

const {
  idNumber,
  queryParams,
  strUsername,
  strEmail,
  strPassword,
  checkToken
} = require('../../utils/validatorUtils');

const Validator = {
  queryParams,
  checkToken,
  idParam: idNumber()
    .required()
    .description('id is required'),
  createUser: Joi.object()
    .keys({
      name: Joi.string(),
      avatar: Joi.string(),
      username: strUsername().alphanum(),
      email: strEmail(),
      password: strPassword(),
      roleId: Joi.number().min(1),
      nationality: Joi.string(),
      job: Joi.string(),
      facebook: Joi.string(),
      howToKnowEnouvoSpace: Joi.string(),
      comment: Joi.string(),
      phoneNumber: Joi.string()
    })
    .options({
      allowUnknown: true
    }),

  updateUser: Joi.object()
    .keys({
      name: Joi.string(),
      avatar: Joi.string(),
      username: strUsername().alphanum(),
      email: strEmail(),
      password: strPassword(),
      roleId: Joi.number().min(1),
      nationality: Joi.string(),
      job: Joi.string(),
      facebook: Joi.string(),
      howToKnowEnouvoSpace: Joi.string(),
      comment: Joi.string(),
      phoneNumber: Joi.string()
    })
    .options({
      allowUnknown: true
    }),

  resetPassword: Joi.object()
    .keys({
      verificationCode: Joi.string().required(),
      password: strPassword().required()
    })
    .options({
      allowUnknown: true
    })
};

module.exports = Validator;

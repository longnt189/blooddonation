const path = require('path');
const CustomModel = require('./CustomModel');

class BloodDonation extends CustomModel {
  static get tableName() {
    return 'bloodDonation';
  }

  static get relationMappings() {
    return {
      donatePlace: {
        relation: CustomModel.HasOneRelation,
        modelClass: path.join(__dirname, '/DonatePlace'),
        join: {
          from: 'bloodDonation.donatePlaceId',
          to: 'donatePlace.id'
        }
      },
      user: {
        relation: CustomModel.HasOneRelation,
        modelClass: path.join(__dirname, '/User'),
        join: {
          from: 'bloodDonation.userId',
          to: 'user.id'
        }
      }
    };
  }
}

module.exports = BloodDonation;

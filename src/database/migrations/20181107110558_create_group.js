exports.up = knex =>
  knex.schema.createTable('group', table => {
    table.increments('id').primary();
    table.string('name');
    table.integer('memberCount');
    table.integer('donateCount');
    table.integer('rareBloodCount');
    table.text('note');
    table.timestamp('createdAt');
    table.timestamp('updatedAt');
  });

exports.down = knex => knex.schema.dropTableIfExists('group');

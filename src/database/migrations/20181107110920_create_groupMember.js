exports.up = knex =>
  knex.schema.createTable('groupMember', table => {
    table.increments('id').primary();
    table.integer('userId');
    table
      .foreign('userId')
      .references('users.id')
      .onDelete('CASCADE');
    table.integer('groupId');
    table
      .foreign('groupId')
      .references('group.id')
      .onDelete('CASCADE');
    table.timestamp('createdAt');
    table.timestamp('updatedAt');
  });

exports.down = knex => knex.schema.dropTableIfExists('groupMember');

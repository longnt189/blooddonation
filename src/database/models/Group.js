const path = require('path');
const CustomModel = require('./CustomModel');

class Group extends CustomModel {
  static get tableName() {
    return 'group';
  }

  $beforeInsert() {
    this.createdAt = new Date().toISOString();
    this.updatedAt = new Date().toISOString();
  }

  $beforeUpdate() {
    this.updatedAt = new Date().toISOString();
  }

  static get relationMappings() {
    return {
      members: {
        relation: CustomModel.HasManyRelation,
        modelClass: path.join(__dirname, '/GroupMember'),
        join: {
          from: 'group.id',
          to: 'groupMember.groupId'
        }
      }
    };
  }
}

module.exports = Group;
